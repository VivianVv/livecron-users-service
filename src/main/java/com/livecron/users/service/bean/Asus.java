package com.livecron.users.service.bean;

public class Asus {
//    public void print(){
//        System.out.println("I am a bean, Asus");
//    }

    private String description;
    private int price;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
