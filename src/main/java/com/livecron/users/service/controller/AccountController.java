package com.livecron.users.service.controller;

import com.livecron.users.service.bean.Asus;
import com.livecron.users.service.input.AccountCreateInput;
import com.livecron.users.service.model.domain.Account;
import com.livecron.users.service.model.domain.AccountState;
import com.livecron.users.service.model.repository.AccountRepository;
import com.livecron.users.service.service.AccountCreateService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;

@RequestMapping(value = "/accounts")
@RequestScope
@RestController
public class AccountController {

    //@Autowired //Dependence Injection por propiedad
    private AccountRepository accountRepository;

    private AccountCreateService accountCreateService;
    //@Autowired
    private Asus asus;

    public AccountController(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public void setAccountRepository(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public void setAsus(Asus asus) {
        this.asus = asus;
    }

    @RequestMapping(method = RequestMethod.POST)
    private Account createAccount(@RequestBody AccountCreateInput input) {
        accountCreateService.setInput(input);
        accountCreateService.execute();

        return accountCreateService.getAccount();
    }

    @RequestMapping(
            value = "/data",
            method = RequestMethod.GET
    )
    public Account findAccountByEmailAndState(@RequestParam("email") String email,
                                              @RequestParam("state") AccountState state) {
        return accountRepository.findByEmailAndState(email, state).orElse(null);
    }

    @RequestMapping(
            value = "/orderBy",
            method = RequestMethod.GET
    )
    public List<Account> findByEmailOrStateOrderByEmailAsc(@RequestParam("email") String email,
                                                           @RequestParam("state") AccountState state) {
        return accountRepository.findAllByEmailOrStateOrderByEmailAsc(email, state);
    }

//    @RequestMapping(
//            value = "/bean",
//            method = RequestMethod.GET
//    )
//    public Asus findAsus{
//        System.out.println("Price: " + asus.getPrice());
//        System.out.println("Descripcion: "+ asus.getDescription());
//        asus.setPrice(12000);
//        return asus;
//    }
//
//    @RequestMapping(
//            value = "/beanSecond",
//            method = RequestMethod.GET
//    )
//    public Asus findAsusSecond{
//        System.out.println("Price: " + asus.getPrice());
//        System.out.println("Descripcion: "+ asus.getDescription());
//        return asus;
//    }
}
