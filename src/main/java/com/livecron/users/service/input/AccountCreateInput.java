package com.livecron.users.service.input;

import com.livecron.users.service.model.domain.AccountState;

public class AccountCreateInput {
    private String email;
    private AccountState accountState;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public AccountState getAccountState() {
        return accountState;
    }

    public void setAccountState(AccountState accountState) {
        this.accountState = accountState;
    }
}
