package com.livecron.users.service.model;

import com.livecron.users.service.bean.Asus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * @author Vivian Valencia
 */
@Configuration
public class Config {
    @Bean()
    @Scope("singleton")
    public Asus asus() {
        Asus asus = new Asus();
        asus.setDescription("I am a bean, ASUS");
        return asus;
    }
}
