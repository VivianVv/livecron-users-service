package com.livecron.users.service.model.domain;

public enum AccountState {
    ACTIVATED,
    DEACTIVATED
}
