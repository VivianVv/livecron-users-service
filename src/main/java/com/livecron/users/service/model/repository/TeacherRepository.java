package com.livecron.users.service.model.repository;

import com.livecron.users.service.model.domain.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

/**
 * @author Vivian Valencia
 */

@Service
public interface TeacherRepository extends JpaRepository<Teacher, Long> {

}
